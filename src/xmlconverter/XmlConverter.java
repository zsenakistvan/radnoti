package xmlconverter;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlConverter<T>{
    
    public Boolean mentesBarmilyenTermekre(T aru){
        String filenev = aru.getClass().getSimpleName() + ".xml";
        File file = new File(filenev);
        Field[] tulajdonsagok = aru.getClass().getFields();
        System.out.println(filenev);
        System.out.println(Arrays.toString(tulajdonsagok));    //prints the resultant array  
//mentést befejezni!!!!!!!
        return Boolean.FALSE;
    }
    
    
    
    
    
    public static void main(String[] args) {
        
        
        Csoki cs = new Csoki(123, "Eperjó", Boolean.FALSE);
        cs.setAr(145);
        cs.osszeg(10);
        cs.getCukormentes();
        
        Whisky ww = new Whisky("fdgd", "eg", 1, 1, 0.1, 0.1);
        ww.osszeg(12);
        ww.setAr(233);
        
        
        
        /*
        //Termek termek = new Termek("fgdfgdf", 232344); 
        
        File file = new File("XmlDate.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        List<Whisky> whiskyk = new ArrayList<>();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(file);
            xml.normalize();
            NodeList whiskyNodeok = xml.getElementsByTagName("whisky");
            for(Integer i = 0; i < whiskyNodeok.getLength(); i++){
                Node whiskyNode = whiskyNodeok.item(i);
                Element whiskyElement = (Element)whiskyNode;
                String orszag = whiskyElement.getElementsByTagName("orszag").item(0).getTextContent();
                String nev = whiskyElement.getElementsByTagName("nev").item(0).getTextContent();
                String korS = whiskyElement.getElementsByTagName("kor").item(0).getTextContent();
                Integer kor = Integer.parseInt(korS);
                String arS = whiskyElement.getElementsByTagName("ar").item(0).getTextContent();
                Integer ar = Integer.parseInt(arS);
                String kiszerelesS = whiskyElement.getElementsByTagName("kiszereles").item(0).getTextContent();
                Double kiszereles = Double.parseDouble(kiszerelesS);
                String alkoholfokS = whiskyElement.getElementsByTagName("alkoholfok").item(0).getTextContent();
                Double alkoholfok = Double.parseDouble(alkoholfokS);
                Whisky w = new Whisky(orszag, nev, kor, ar, kiszereles, alkoholfok);
                whiskyk.add(w);
            }
            
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        
        
        for(Whisky whisky : whiskyk){
            System.out.println(whisky.getNev());
        }
        
        
        Scanner scn = new Scanner(System.in);
        System.out.print("Add meg, melyik országból származik a whisky: ");
        String wOrszag = scn.nextLine();
        System.out.print("Add meg a whisky márkáját: ");
        String wMarka = scn.nextLine();
        System.out.print("Add meg a whisky korát: ");
        String wKor = scn.nextLine();
        System.out.print("Add meg a whisky árát: ");
        String wAr = scn.nextLine();
        System.out.print("Add meg a whisky kiszerelését: ");
        String wKiszereles = scn.nextLine();
        System.out.print("Add meg a whisky alkoholfokát: ");
        String wAlkohol = scn.nextLine();
        
        try{
            Integer kor = Integer.parseInt(wKor);
            Integer ar = Integer.parseInt(wAr);

            Double kiszereles = Double.parseDouble(wKiszereles);
            Double alkohol = Double.parseDouble(wAlkohol);
            
            Whisky w = new Whisky(wOrszag, wMarka, kor, ar, kiszereles, alkohol);
            
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(file);
            xml.normalize();
            
            Element ujWhisky = xml.createElement("whisky");
            
            Element ujOrszag = xml.createElement("orszag");
            ujOrszag.setTextContent(w.getOrszag());
            ujWhisky.appendChild(ujOrszag);
            
            Element ujNev = xml.createElement("nev");
            ujWhisky.appendChild(ujNev);
            ujNev.setTextContent(w.getNev());
            
            Element ujKor = xml.createElement("kor");
            ujKor.setTextContent(w.getKor().toString());
            ujWhisky.appendChild(ujKor);
            
            Element ujAr = xml.createElement("ar");
            ujAr.setTextContent(w.getAr().toString());
            ujWhisky.appendChild(ujAr);
            
            Element ujKiszereles = xml.createElement("kiszereles");
            ujKiszereles.setTextContent(w.getKiszereles().toString());
            ujWhisky.appendChild(ujKiszereles);
            
            Element ujAlkohol = xml.createElement("alkoholfok");
            ujAlkohol.setTextContent(w.getAlkoholfok().toString());
            ujWhisky.appendChild(ujAlkohol);
            
            xml.getFirstChild().appendChild(ujWhisky);
            
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            StreamResult s = new StreamResult(file);
            DOMSource d = new DOMSource(xml);
            t.transform(d, s);
            
            
            
        }
        catch(Exception ex){
            System.err.println("Hiba: " + ex.toString());
            System.err.println("A hiba oka: " + Arrays.toString(ex.getStackTrace()));
        }
        
        
        
    */    
    }
    
}
