package xmlconverter;
public class Whisky extends Termek{
    public String orszag;
    public Integer kor;
    public Double kiszereles;
    public Double alkoholfok;

    public Whisky(String orszag, String nev, Integer kor, Integer ar, Double kiszereles, Double alkoholfok) {
        super(nev, ar);
        this.orszag = orszag;
        this.kor = kor;
        this.kiszereles = kiszereles;
        this.alkoholfok = alkoholfok;
    }

    public String getOrszag() {
        return orszag;
    }

    public Integer getKor() {
        return kor;
    }

    public Double getKiszereles() {
        return kiszereles;
    }

    public Double getAlkoholfok() {
        return alkoholfok;
    }
    
    @Override
    public void osszeg(Integer darab){
        Double ar = (this.ar * 1.27) * darab;
        System.out.println("A Whisky-k adóval terhelt összaár: " + ar + " Ft");
    }
     
}
