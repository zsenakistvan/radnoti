
package xmlconverter;

public abstract class Termek {
    public String nev;
    public Integer ar;

    public Termek(String nev, Integer ar) {
        this.nev = nev;
        this.ar = ar;
    }

    public String getNev() {
        return this.nev;
    }

    public Integer getAr() {
        return this.ar;
    }

    public void setAr(Integer ar) {
        this.ar = ar;
    }
    
    public void osszeg(Integer darab){
        System.out.println("A termékek összára: " + (darab * this.ar) + " Ft");
    }
    
}
