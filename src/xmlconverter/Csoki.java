/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlconverter;

/**
 *
 * @author zsenakistvan
 */
public class Csoki extends Termek{
    
    public Boolean cukormentes; 

    public Csoki(Integer ar, String nev, Boolean cukormentes) {
        super(nev, ar);
        this.cukormentes = cukormentes;
    }

    public Boolean getCukormentes() {
        return this.cukormentes;
    }
   
}
